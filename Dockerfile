# Emby Server
FROM ubuntu:19.10
ENV APP_NAME="emby-server" IMG_NAME="embyserver-hwaccel" EDGE=0 UMASK=002 VERSION=4.3.1.0 \
 UID=1000 \
 GID=1000 \
 GIDLIST=1000

ADD https://github.com/just-containers/s6-overlay/releases/download/v1.22.1.0/s6-overlay-amd64.tar.gz /var/tmp/
ADD https://github.com/MediaBrowser/Emby.Releases/releases/download/${VERSION}/emby-server-deb_${VERSION}_amd64.deb /var/tmp/

RUN apt -qq update \
 && apt -qqy install --no-install-recommends va-driver-all \
 && dpkg-deb -R /var/tmp/emby-server-deb_${VERSION}_amd64.deb /var/tmp/emby-server/ \
 && echo "Stripping emby-server-deb_${VERSION}_amd64.deb of unneeded files" \
 && rm -rf /var/tmp/emby-server/etc/firewalld \
            /var/tmp/emby-server/etc/init /var/tmp/emby-server/bin/emby-server  \
            /var/tmp/emby-server/usr/lib/ \
            /var/tmp/emby-server/etc/emby-server.conf \
            /var/tmp/emby-server/DEBIAN/conffiles \
            /var/tmp/emby-server/DEBIAN/postinst \
            /var/tmp/emby-server/DEBIAN/postrm \
            /var/tmp/emby-server/DEBIAN/prerm \
 && dpkg-deb -b /var/tmp/emby-server/ /var/tmp/emby-server-stripped.deb \
 && apt -qqy install /var/tmp/emby-server-stripped.deb  \
 && tar xzf /var/tmp/s6-overlay-amd64.tar.gz -C / \
 && rm -rf /var/tmp/emby-server/ /tmp/* /var/tmp/* \
 && rm -rf /var/lib/apt/lists/

ADD run /etc/services.d/emby-server/

VOLUME [ "/config" ]
EXPOSE 8096 8920 7359/udp 1900/udp

ENTRYPOINT ["/init"]
